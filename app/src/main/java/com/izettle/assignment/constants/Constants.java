package com.izettle.assignment.constants;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.GRID;
import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.RANDOM;
import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.VERTICAL;

public class Constants {
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({VERTICAL, GRID, RANDOM})
    public @interface VIEW_CONSTANTS {
        final int VERTICAL = 1;
        final int GRID = 0;
        final int RANDOM = 3;
    }
}
