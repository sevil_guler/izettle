package com.izettle.assignment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.izettle.assignment.R;
import com.izettle.assignment.model.DummyItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.GRID;
import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.RANDOM;
import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.VERTICAL;

public class RecylerListViewAdapter extends RecyclerView.Adapter<RecylerListViewAdapter.DummyViewHolder> {
    int view_type;
    ArrayList<DummyItem> items;

    public RecylerListViewAdapter(ArrayList<DummyItem> items, int view_type) {
        this.items = items;
        this.view_type = view_type;
    }

    public void setView_type(int view_type) {
        this.view_type = view_type;
    }

    @Override
    public DummyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case VERTICAL:
                view = inflater.inflate(R.layout.list_item_vertical_representation, parent, false);
                break;
            case GRID:
            default:
                view = inflater.inflate(R.layout.list_item, parent, false);
                break;
        }
        return new DummyViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        if (view_type != RANDOM) {
            return view_type;
        } else {
            return items.get(position).getRepresentationType();
        }
    }

    @Override
    public void onBindViewHolder(DummyViewHolder holder, int position) {
        DummyItem item = items.get(position);
        holder.item_image.setBackgroundResource(item.getResId());
        holder.title.setText(item.getTitle());
        holder.description.setText(item.getDescription());
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class DummyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.item_image)
        ImageView item_image;

        public DummyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            title.setOnClickListener(this);
            description.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            int id = view.getId();
            switch (id) {
                case R.id.title:
                    Toast.makeText(context, this.title.getText().toString(), Toast.LENGTH_SHORT).show();
                    break;
                case R.id.description:
                    Toast.makeText(context, this.description.getText().toString(), Toast.LENGTH_SHORT).show();
                    break;

            }

        }
    }

}
