package com.izettle.assignment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class DummyItem {
    @NonNull
    int resId;
    @NonNull
    String title;
    @NonNull
    String description;
    //This variable shouldn't be here actually . Model should be completely seperated from UI but that is just dummy argument of dummy item
    @NonNull
    int representationType;
}
