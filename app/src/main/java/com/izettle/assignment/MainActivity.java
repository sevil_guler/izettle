package com.izettle.assignment;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import com.izettle.assignment.adapter.RecylerListViewAdapter;
import com.izettle.assignment.model.DummyItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.GRID;
import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.RANDOM;
import static com.izettle.assignment.constants.Constants.VIEW_CONSTANTS.VERTICAL;

public class MainActivity extends AppCompatActivity {
    private final int COLUMNSPAN = 3;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ArrayList<DummyItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        createDummyList();
        initializeAdapter();
    }

    private void initializeAdapter() {
        RecylerListViewAdapter mAdapter = new RecylerListViewAdapter(items, GRID);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(mAdapter);
    }

    private void createDummyList() {
        items = new ArrayList<DummyItem>();
        Resources res = getResources();
        items.add(new DummyItem(R.drawable.dummy_pic1, "1."+res.getString(R.string.title), res.getString(R.string.description), VERTICAL));
        items.add(new DummyItem(R.drawable.dummy_pic2, "2."+res.getString(R.string.title), res.getString(R.string.description), GRID));
        items.add(new DummyItem(R.drawable.dummy_pic3, "3."+res.getString(R.string.title), res.getString(R.string.description_short), VERTICAL));
        items.add(new DummyItem(R.drawable.dummy_pic3, "4."+res.getString(R.string.title), res.getString(R.string.description), VERTICAL));
        items.add(new DummyItem(R.drawable.dummy_pic2, "5."+res.getString(R.string.title), res.getString(R.string.description), GRID));
        items.add(new DummyItem(R.drawable.dummy_pic1, "6."+res.getString(R.string.title), res.getString(R.string.description_short), VERTICAL));
        items.add(new DummyItem(R.drawable.dummy_pic1, "7."+res.getString(R.string.title), res.getString(R.string.description), VERTICAL));
        items.add(new DummyItem(R.drawable.dummy_pic2, "8."+res.getString(R.string.title), res.getString(R.string.description), GRID));
        items.add(new DummyItem(R.drawable.dummy_pic3, "9."+res.getString(R.string.title), res.getString(R.string.description), VERTICAL));
        items.add(new DummyItem(R.drawable.dummy_pic3, "10."+res.getString(R.string.title), res.getString(R.string.description_short), VERTICAL));
        items.add(new DummyItem(R.drawable.dummy_pic2, "11."+res.getString(R.string.title), res.getString(R.string.description), GRID));
        items.add(new DummyItem(R.drawable.dummy_pic1, "12."+res.getString(R.string.title), res.getString(R.string.description_short), GRID));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int resId = item.getItemId();
        switch (resId) {
            case R.id.linear_view:
                updateViewType(new LinearLayoutManager(getApplicationContext()), GRID);
                break;
            case R.id.grid_view:
                updateViewType(new GridLayoutManager(getApplicationContext(), 2), VERTICAL);
                break;
            case R.id.random:
                updateViewType(new LinearLayoutManager(getApplicationContext()), RANDOM);
                break;
            default:
                int orientation = (int) (Math.random() * (2));
                updateViewType(new StaggeredGridLayoutManager(COLUMNSPAN, orientation), orientation);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void updateViewType(RecyclerView.LayoutManager layoutManager, int view_type) {
        recyclerView.setLayoutManager(layoutManager);
        ((RecylerListViewAdapter) recyclerView.getAdapter()).setView_type(view_type);
    }


}
